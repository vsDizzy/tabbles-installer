﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using ShellMenu.Interop.Shell;
using ShellMenu.Interop.Win;

namespace ShellMenu
{
	/// <summary>
	///     Base class that implements Shell context menu logic.
	///     Derive from it and implement abstract members.
	///     Also make derived class COM visible and assign a Guid to it.
	/// </summary>
	public abstract class ContextMenuBase : IShellExtInit, IContextMenu3
	{
		private int _firstCmdId;
		private MenuItem[] _items;

		public int QueryContextMenu(IntPtr hMenu, int indexMenu, int idCmdFirst, int idCmdLast, int flags)
		{
			_items = InitMenu();

			_firstCmdId = idCmdFirst;

			foreach (MenuItem item in _items)
			{
				bool isWindowsXp = Environment.OSVersion.Version.Major < 6;

				if (isWindowsXp)
				{
					var mi = new MenuInfo {Size = Marshal.SizeOf(typeof (MenuInfo)), Mask = MenuInfoMask.Style};
					if (WinApi.GetMenuInfo(hMenu, ref mi))
					{
						mi.Style |= MenuInfoStyle.CheckOrBmp;
						WinApi.SetMenuInfo(hMenu, ref mi);
					}
				}

				const int ownerDrawBitmap = -1;
				var mii = new MenuItemInfo
				{
					Size = Marshal.SizeOf(typeof (MenuItemInfo)),
					Mask = MenuItemInfoMask.Id | MenuItemInfoMask.Bitmap | MenuItemInfoMask.String,
					Id = idCmdFirst++,
					hBmpItem =
						isWindowsXp
							? new IntPtr(ownerDrawBitmap)
							: item.Bitmap.GetHbitmap(Color.FromArgb(255, 0, 0, 0)),
					TypeData = item.Caption
				};

				WinApi.InsertMenuItem(hMenu, indexMenu++, true, ref mii);
			}

			const int severitySuccess = 0;
			return WinApi.MakeHResult(severitySuccess, 0, idCmdFirst - _firstCmdId);
		}

		public void InvokeCommand(ref CmInvokeCommandInfo ici)
		{
			var verb = (int) ici.Verb;
			int hiWord = verb >> 16;
			if (hiWord == 0 && verb >= 0 && verb < _items.Length)
			{
				Invoke(_items[verb].Verb, ici.hWnd);
			}
		}

		public void GetCommandString(IntPtr idCmd, GetCommandStringFlags type, IntPtr reserved, IntPtr name, int chMax)
		{
			var cmd = (int) idCmd;
			if (cmd >= 0 && cmd < _items.Length)
			{
				byte[] bytes;
				switch (type)
				{
					case GetCommandStringFlags.VerbA:
						bytes = Encoding.ASCII.GetBytes(_items[cmd].Verb);
						Marshal.Copy(bytes, 0, name, Math.Min(bytes.Length, chMax));
						break;
					case GetCommandStringFlags.VerbW:
						bytes = Encoding.Unicode.GetBytes(_items[cmd].Verb);
						Marshal.Copy(bytes, 0, name, Math.Min(bytes.Length, chMax));
						break;
				}
			}
		}

		public void HandleMenuMsg(int msg, IntPtr wParam, IntPtr lParam)
		{
			HandleMenuMsg2(msg, wParam, lParam, IntPtr.Zero);
		}

		public void HandleMenuMsg2(int msg, IntPtr wParam, IntPtr lParam, IntPtr resultPtr)
		{
			const int wmMeasureitem = 0x2c;
			const int wmDrawitem = 0x2b;

			int result = 0;

			switch (msg)
			{
				case wmMeasureitem:
					var mis = (MeasureItemStruct) Marshal.PtrToStructure(lParam, typeof (MeasureItemStruct));
					mis.ItemWidth = mis.ItemHeight; // Makes icon's width same as height.
					Marshal.StructureToPtr(mis, lParam, false);
					result = 1;
					break;

				case wmDrawitem:
					var dis = (DrawItemStruct) Marshal.PtrToStructure(lParam, typeof (DrawItemStruct));

					// Draw bitmap.
					Graphics gr = Graphics.FromHdc(dis.hDc);
					int index = dis.ItemId - _firstCmdId;
					Rect rc = dis.RcItem;
					var rectangle = new Rectangle(rc.Left, rc.Top, rc.Right - rc.Left, rc.Bottom - rc.Top);
					gr.DrawImage(_items[index].Bitmap, rectangle);
					gr.Dispose();
					result = 1;
					break;
			}

			if (resultPtr != IntPtr.Zero)
			{
				Marshal.WriteInt32(resultPtr, result);
			}
		}

		public void Initialize(IntPtr pidlFolder, IDataObject dtObj, IntPtr hKeyProgId)
		{
			HandleSelection(GetDropObjectsFilenames(dtObj));
		}

		/// <summary>Override this method to get selected files.</summary>
		protected abstract void HandleSelection(string[] fileNames);

		protected abstract MenuItem[] InitMenu();

		protected abstract void Invoke(string verb, IntPtr hWnd);

		/// <summary>Gets array of selected files.</summary>
		private static string[] GetDropObjectsFilenames(IDataObject dataObject)
		{
			const short cfHdrop = 15;
			var fmt = new FORMATETC
			{
				cfFormat = cfHdrop,
				dwAspect = DVASPECT.DVASPECT_CONTENT,
				lindex = -1,
				tymed = TYMED.TYMED_HGLOBAL
			};

			STGMEDIUM stg;

			dataObject.GetData(ref fmt, out stg);

			try
			{
				IntPtr hDrop = WinApi.GlobalLock(stg.unionmember);
				try
				{
					int numFiles = WinApi.DragQueryFile(hDrop, -1, null, 0);

					var files = new string[numFiles];
					var sb = new StringBuilder(0x4000);
					for (int i = 0; i < numFiles; i++)
					{
						WinApi.DragQueryFile(hDrop, i, sb, sb.Capacity);
						files[i] = sb.ToString();
					}
					return files;
				}
				finally
				{
					WinApi.GlobalUnlock(stg.unionmember);
				}
			}
			finally
			{
				WinApi.ReleaseStgMedium(ref stg);
			}
		}
	}
}