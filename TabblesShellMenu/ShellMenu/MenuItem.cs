﻿using System.Drawing;

namespace ShellMenu
{
	/// <summary>Shell context menu item.</summary>
	public class MenuItem
	{
		public Bitmap Bitmap;
		public string Caption;
		public string Verb;
	}
}