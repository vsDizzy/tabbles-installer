﻿using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Win
{
	/// <summary>Windows RECT structure.</summary>
	[StructLayout(LayoutKind.Sequential)]
	internal struct Rect
	{
		public int Left;
		public int Top;
		public int Right;
		public int Bottom;
	}
}