﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Win
{
	/// <summary>Windows MEASUREITEMSTRUCT structure.</summary>
	[StructLayout(LayoutKind.Sequential)]
	internal struct MeasureItemStruct
	{
		public int CtlType;
		public int CtlId;
		public int ItemId;
		public int ItemWidth;
		public int ItemHeight;
		public IntPtr ItemData;
	}
}