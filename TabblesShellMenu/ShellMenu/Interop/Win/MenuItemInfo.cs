﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Win
{
	/// <summary>Windows MENUITEMINFO structure.</summary>
	[StructLayout(LayoutKind.Sequential)]
	internal struct MenuItemInfo
	{
		public int Size;
		public MenuItemInfoMask Mask;
		public int Type;
		public int State;
		public int Id;
		public IntPtr hSubMenu;
		public IntPtr hBmpChecked;
		public IntPtr hBmpUnchecked;
		public IntPtr ItemData;
		public string TypeData;
		public int cCh;
		public IntPtr hBmpItem;
	}
}