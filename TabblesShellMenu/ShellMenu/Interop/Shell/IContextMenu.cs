﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Shell
{
	/// <summary>IContextMenu Shell interface.</summary>
	[ComImport, Guid("000214e4-0000-0000-c000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IContextMenu
	{
		[PreserveSig]
		int QueryContextMenu(IntPtr hMenu, int indexMenu, int idCmdFirst, int idCmdLast, int flags);

		void InvokeCommand([In] ref CmInvokeCommandInfo ici);
		void GetCommandString(IntPtr idCmd, GetCommandStringFlags type, IntPtr reserved, IntPtr name, int chMax);
	}
}