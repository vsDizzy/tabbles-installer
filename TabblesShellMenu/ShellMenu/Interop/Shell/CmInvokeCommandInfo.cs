﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Shell
{
	/// <summary>CMINVOKECOMMANDINFO Shell structure.</summary>
	/// <see cref="IContextMenu" />
	[StructLayout(LayoutKind.Sequential)]
	public struct CmInvokeCommandInfo
	{
		public int Size;
		public int Mask;
		public IntPtr hWnd;
		public IntPtr Verb;
		public IntPtr Parameters;
		public IntPtr Directory;
		public int Show;
		public int HotKey;
		public IntPtr hIcon;
	}
}