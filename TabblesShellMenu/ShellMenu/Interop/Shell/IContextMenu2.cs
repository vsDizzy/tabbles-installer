﻿using System;
using System.Runtime.InteropServices;

namespace ShellMenu.Interop.Shell
{
	/// <summary>IContextMenu2 Shell interface.</summary>
	/// <see cref="IContextMenu" />
	[ComImport, Guid("000214f4-0000-0000-c000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface IContextMenu2 : IContextMenu
	{
		[PreserveSig]
		new int QueryContextMenu(IntPtr hMenu, int indexMenu, int idCmdFirst, int idCmdLast, int flags);

		new void InvokeCommand([In] ref CmInvokeCommandInfo ici);
		new void GetCommandString(IntPtr idCmd, GetCommandStringFlags type, IntPtr reserved, IntPtr name, int chMax);

		void HandleMenuMsg(int msg, IntPtr wParam, IntPtr lParam);
	}
}