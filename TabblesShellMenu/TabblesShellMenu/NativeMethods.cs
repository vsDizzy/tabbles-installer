﻿using System;
using System.Runtime.InteropServices;

namespace TabblesShellMenu
{
	public abstract class NativeMethods
	{
		public const int WmCopydata = 0x004a;

		[DllImport("user32.dll", EntryPoint = "FindWindow")]
		public static extern IntPtr FindWindow(string lpClasName, string lpWindowName);

		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass,
			string lpszWindow);

		[DllImport("user32.dll", EntryPoint = "SendMessage")]
		public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern bool PostMessage(IntPtr hWnd, int msg, int wParam, int lParam);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern bool ShowWindow(IntPtr hWnd, int cmdShow);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern bool ShowWindowAsync(IntPtr hWnd, int cmdShow);

		[DllImport("user32.dll")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		[StructLayout(LayoutKind.Sequential)]
		public struct CopyDataStruct
		{
			public string ID;
			public int Length;
			public string Data;
		}
	}
}