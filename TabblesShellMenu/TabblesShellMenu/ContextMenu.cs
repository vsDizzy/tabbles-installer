﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using TabblesShellMenu.Localization;
using TabblesShellMenu.Properties;
using MenuItem = ShellMenu.MenuItem;

namespace TabblesShellMenu
{
	[ComVisible(true), Guid("CF13206F-8D94-4A66-91B4-402C17075828"), ClassInterface(ClassInterfaceType.None)]
	public class ContextMenu : RegistrationHelper
	{
		private string[] _files;

		[ComRegisterFunction]
		public new static void Register(Type type)
		{
			RegistrationHelper.Register(type);
		}

		[ComUnregisterFunction]
		public new static void Unregister(Type type)
		{
			RegistrationHelper.Unregister(type);
		}

		protected override void HandleSelection(string[] fileNames)
		{
			_files = fileNames;
		}

		protected override MenuItem[] InitMenu()
		{
			bool tutteDir = true;

			foreach (string file in _files)
			{
				if (!Directory.Exists(file))
				{
					tutteDir = false;
				}
			}

			string stringaTag = "";
			string stringaUntag = "";

			if (_files.Length == 1)
			{
				if (tutteDir)
				{
					stringaTag = Strings.tagFolder;
					stringaUntag = Strings.untagFolder;
				}
				else
				{
					stringaTag = Strings.tagFile;
					stringaUntag = Strings.untagFile;
				}
			}
			else
			{
				string lengthStr = _files.Length.ToString(CultureInfo.InvariantCulture);
				if (tutteDir)
				{
					stringaTag = Strings.tagFolders.Replace("(N)", lengthStr);
					stringaUntag = Strings.untagFolders.Replace("(N)", lengthStr);
				}
				else
				{
					stringaTag = Strings.tagFiles.Replace("(N)", lengthStr);
					stringaUntag = Strings.untagFiles.Replace("(N)", lengthStr);
				}
			}

			var items = new List<MenuItem>
			{
				new MenuItem {Bitmap = Resources.Tag, Caption = stringaTag, Verb = "categorize"}
			};

			if (_files.Length == 1)
			{
				if (tutteDir)
				{
					items.Add(new MenuItem
					{
						Bitmap = Resources.Goto,
						Caption = Strings.openFolder,
						Verb = "openFolder"
					});
				}
				else
				{
					items.Add(new MenuItem
					{
						Bitmap = Resources.Goto,
						Caption = Strings.locate,
						Verb = "locate"
					});
				}
			}

			return items.ToArray();
		}

		protected override void Invoke(string verb, IntPtr hWnd)
		{
			switch (verb)
			{
				case "categorize":
					CoreSendMessageToTabbles(true, hWnd);
					break;
				case "uncategorize":
					CoreSendMessageToTabbles(false, hWnd);
					break;
				case "locate":
					CoreLocateInTabbles(hWnd, Command.LocateInTabbles, _files[0]);
					break;
				case "openFolder":
					CoreLocateInTabbles(hWnd, Command.OpenDirInTabbles, _files[0]);
					break;
			}
		}

		private bool CoreSendMessageToTabbles(bool categ, IntPtr explorerHwnd)
		{
			//var exwin = e.HWnd;
			//MessageBox.Show("handle = " + win.ToString());
			//System.Console.WriteLine("Shutting down current Tabbles instance, if any...");
			//var procs = System.Diagnostics.Process.GetProcessesByName("tabbles");
			//if (procs.Length == 0)
			//    MessageBox.Show("Tabbles is not active. Please start Tabbles first.", "Tabbles not active.", MessageBoxButtons.OK, MessageBoxIcon.Information);

			try
			{
				Process[] procs = Process.GetProcessesByName("tabbles");
				if (procs.Length == 0)
				{
					MessageBox.Show(Strings.tabbles_not_running, Strings.tabnr, MessageBoxButtons.OK,
						MessageBoxIcon.Information);
				}
				else
				{
					string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

					string path_quick_wnd = Path.Combine(dir, "tabbles_hwnd_quick_link");
					FileStream fil1 = File.OpenRead(path_quick_wnd);
					var sr1 = new StreamReader(fil1);
					string qwp = sr1.ReadLine();
					int quick_handle = Int32.Parse(qwp);
					sr1.Close();
					fil1.Close();


					// scrivi il file che contiene il messaggio da passare a tabbles

					string fpath = Path.Combine(dir, "tabbles_message2");
					FileStream fs = File.Open(fpath, FileMode.Create);
					var sw = new StreamWriter(fs);
					sw.WriteLine(explorerHwnd.ToString()); // prima scrivi la mia handle
					sw.WriteLine(categ.ToString());
					foreach (string file in _files)
					{
						sw.WriteLine(file);
					}

					sw.Close();
					fs.Close();

					var qh = new IntPtr(quick_handle);
					NativeMethods.ShowWindowAsync(qh, 5); //SW_show = 5 . useful in case Tabbles is hidden in tray area
					// qui dovrei fare restore, non minimize
					NativeMethods.SetForegroundWindow(qh); // bring tabbles on top
					// informa tabbles che ho scritto il file
					NativeMethods.PostMessage(qh, 0x7ff1, 0, 1);
				}
			}
			catch (FileNotFoundException)
			{
				MessageBox.Show(Strings.tabbles_not_running, Strings.tabnr, MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
			catch (Exception ecc)
			{
				MessageBox.Show(ecc.Message + " - " + ecc.GetType(), "Error");
			}

			return true; // era un mio verbo
		}

		private bool CoreLocateInTabbles(IntPtr explorerHwnd, Command cmd, string path)
		{
			try
			{
				Process[] procs = Process.GetProcessesByName("tabbles");
				if (procs.Length == 0)
				{
					MessageBox.Show(Strings.tabbles_not_running, Strings.tabnr, MessageBoxButtons.OK,
						MessageBoxIcon.Information);
				}
				else
				{
					string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

					string pathMainWnd = Path.Combine(dir, "tabbles_hwnd_main");
					FileStream fil1 = File.OpenRead(pathMainWnd);
					var sr1 = new StreamReader(fil1);
					string qwp = sr1.ReadLine();
					int mainWnd = Int32.Parse(qwp);
					sr1.Close();
					fil1.Close();


					// scrivi il file che contiene il messaggio da passare a tabbles

					string fpath = Path.Combine(dir, "tabbles_message2");
					FileStream fs = File.Open(fpath, FileMode.Create);
					var sw = new StreamWriter(fs);
					sw.WriteLine(explorerHwnd.ToString()); // prima scrivi la mia handle
					if (cmd == Command.LocateInTabbles)
					{
						sw.WriteLine("1");
					}
					else if (cmd == Command.OpenDirInTabbles)
					{
						sw.WriteLine("2");
					}

					sw.WriteLine(path);


					sw.Close();
					fs.Close();


					var qh = new IntPtr(mainWnd);

					// Non mostro la finestra di Tabbles perché dà fastidio, non serve, e fa il bug dello schermo nero se non chiamo Show anche da Tabbles.
					//NativeMethods.ShowWindowAsync(qh, 5); //SW_show = 5 . useful in case Tabbles is hidden in tray area

					// qui dovrei fare restore, non minimize
					NativeMethods.SetForegroundWindow(qh); // bring tabbles on top
					// informa tabbles che ho scritto il file
					NativeMethods.PostMessage(qh, 0x7ff2, 0, 1);
				}
			}
			catch (FileNotFoundException)
			{
				MessageBox.Show(Strings.tabbles_not_running, Strings.tabnr, MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
			catch (Exception ecc)
			{
				MessageBox.Show(ecc.Message + " - " + ecc.GetType(), "Error");
			}

			return true; // era un mio verbo
		}

		private enum Command
		{
			LocateInTabbles,
			OpenDirInTabbles
		}
	}
}