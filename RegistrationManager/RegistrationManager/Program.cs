﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Win32;
namespace RegistrationManager
{
    internal abstract class Program
    {
        private static readonly RegistrationServices RegistrationServices;

        static Program()
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;

            RegistrationServices = new RegistrationServices();
        }

        private static void handleException(Exception e)
        {
            try
            {

                var logPath = System.IO.Path.Combine(Path.GetTempPath(), "tabbles-registration-manager-log.txt");

                using (var tw = new StreamWriter(logPath, true))
                {
                    tw.WriteLine("time = " + DateTime.Now.ToString());
                    tw.WriteLine("exc = " + e.GetType().ToString());
                    tw.WriteLine("exc message = " + e.Message);
                    tw.WriteLine("exc stracktrace = " + e.StackTrace);
                }

            }
            catch { }

        }

        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {

            try
            {
                string name = Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().Location);
                string filename = string.Format("tabbles-registration-manager-crash-{0}.txt", name);
                string path = Path.Combine(Path.GetTempPath(), filename);
                using (var sw = new StreamWriter(path, true))
                {
                    sw.WriteLine("User:{0}, Is64BitProcess: {1}, Time: {2}, Exception: {3}", Environment.UserName,
                        Environment.Is64BitProcess, DateTime.Now, e.ExceptionObject);
                }
            }
            catch { }
        }

        private static void Main(string[] args)
        {
            try
            {
                if (args.Length == 2)
                {
                    string cmd = args[0];
                    string file = args[1];
                    switch (cmd.ToLower(CultureInfo.InvariantCulture))
                    {
                        case "/reg":
                            Register(file);
                            return;
                        case "/unreg":
                            Unregister(file);
                            return;
                        case "/kill":
                            KillProcess(file);
                            return;
                        case "/unlock":
                            Unlock(file);
                            return;
                        case "/regoutlook":
                            registerOutlookAddin(file);
                            return;
                        case "/unregoutlook":
                            unregisterOutlookAddin();
                            return;
                    }
                }

                string module = Path.GetFileName(Assembly.GetExecutingAssembly().Location);
                Console.WriteLine("Usage: {0} [/reg | /unreg | /kill | /unlock] FileName", module);
            }
            catch (Exception e)
            {
                handleException(e);
            }
        }

        private static void registerOutlookAddin(string installFolder)
        {

            var installFolder2 = installFolder.Replace(@"\", @"/");
            var pathInWeirdFormat = @"file:///" + installFolder2 + "/outlook/Tabbles.OutlookAddin.vsto|vstolocal";

            using (var k = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Office\Outlook\Addins\Tabbles"))
            {
                k.SetValue("Description", Resource1.tagEmailsWithTabbles, RegistryValueKind.String);
                k.SetValue("FriendlyName", Resource1.tabbles, RegistryValueKind.String);
                k.SetValue("LoadBehavior", 3, RegistryValueKind.DWord);
                k.SetValue("Manifest", pathInWeirdFormat, RegistryValueKind.String);
            }
            




        }

        private static void unregisterOutlookAddin()
        {


            Registry.CurrentUser.DeleteSubKeyTree(@"Software\Microsoft\Office\Outlook\Addins\Tabbles", throwOnMissingSubKey:false);
            


        }

        private static void Register(string filename)
        {
            Assembly assembly = Assembly.LoadFrom(filename);
            RegistrationServices.RegisterAssembly(assembly, AssemblyRegistrationFlags.SetCodeBase);
        }

        private static void Unregister(string fileName)
        {
            Assembly assembly = Assembly.LoadFrom(fileName);
            RegistrationServices.UnregisterAssembly(assembly);
        }

        private static void KillProcess(string processName)
        {
            Process[] processes = Process.GetProcessesByName(processName);
            foreach (Process process in processes)
            {
                //using (process)
                {
                    process.Kill();
                    
                }
            }
        }

        private static IEnumerable<int> GetModuleProcessIds(string fileName)
        {
            var ids = new List<int>();
            foreach (Process process in Process.GetProcesses())
            {
                using (process)
                {
                    try
                    {
                        foreach (ProcessModule module in process.Modules)
                        {
                            using (module)
                            {
                                if (module.FileName.Equals(fileName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    ids.Add(process.Id);
                                    break;
                                }
                            }
                        }
                    }
                    catch (Win32Exception)
                    {
                    }
                }
            }
            return ids.ToArray();
        }

        private static void Unlock(string fileName)
        {
            foreach (int id in GetModuleProcessIds(fileName))
            {
                using (Process process = Process.GetProcessById(id))
                {
                    process.Kill();
                }
            }
        }
    }
}