﻿using System.Runtime.InteropServices;

namespace FileOperation.Registration.Interop
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct LuidAndAttributes
	{
		public Luid Luid;
		public int Attributes;
	}
}