﻿using System.Runtime.InteropServices;

namespace FileOperation.Registration.Interop
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct Luid
	{
		public int LowPart;
		public int HighPart;
	}
}