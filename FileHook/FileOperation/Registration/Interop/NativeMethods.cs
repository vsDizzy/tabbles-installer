﻿using System;
using System.Runtime.InteropServices;

namespace FileOperation.Registration.Interop
{
	internal static class NativeMethods
	{
		[DllImport("Ole32.dll", CharSet = CharSet.Unicode, PreserveSig = false)]
		public static extern void CoTreatAsClass([MarshalAs(UnmanagedType.LPStruct)] Guid clsidOld,
			[MarshalAs(UnmanagedType.LPStruct)] Guid clsidNew);

		[DllImport("Kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool CloseHandle(IntPtr hObject);

		[DllImport("Advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool OpenProcessToken(IntPtr processHandle, int desiredAccess, out IntPtr tokenHandle);

		[DllImport("Advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool LookupPrivilegeValue([MarshalAs(UnmanagedType.LPWStr)] string lpSystemName,
			[MarshalAs(UnmanagedType.LPWStr)] string lpName, out Luid lpLuid);

		[DllImport("Advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool AdjustTokenPrivileges(IntPtr tokenHandle, bool disableAllPrivileges,
			ref TokenPrivileges newState, int bufferLength, IntPtr previousState, IntPtr returnLength);
	}
}