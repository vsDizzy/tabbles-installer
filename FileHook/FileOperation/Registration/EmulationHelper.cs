﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using FileOperation.Registration.Interop;
using Microsoft.Win32;

namespace FileOperation.Registration
{
	internal static class EmulationHelper
	{
		public static void RegTreatAs(Guid clsidOld, Guid clsidNew)
		{
			AdjustTokenPrivileges();

			string path = string.Format(@"CLSID\{{{0}}}", clsidOld);
			var admin = new SecurityIdentifier(WellKnownSidType.BuiltinAdministratorsSid, null);
			RegistrySetOwner(path, admin);

			NativeMethods.CoTreatAsClass(clsidOld, clsidNew);
		}

		private static void AdjustTokenPrivileges()
		{
			IntPtr hToken;
			const int tokenAdjustPrivileges = 0x0020;
			if (!NativeMethods.OpenProcessToken(Process.GetCurrentProcess().Handle, tokenAdjustPrivileges, out hToken))
			{
				int hr = Marshal.GetHRForLastWin32Error();
				Marshal.ThrowExceptionForHR(hr);
			}

			try
			{
				Luid luid;
				const string seTakeOwnershipName = "SeTakeOwnershipPrivilege";
				if (!NativeMethods.LookupPrivilegeValue(null, seTakeOwnershipName, out luid))
				{
					int hr = Marshal.GetHRForLastWin32Error();
					Marshal.ThrowExceptionForHR(hr);
				}

				const int sePrivilegeEnabled = 0x00000002;

				var privilege = new LuidAndAttributes {Luid = luid, Attributes = sePrivilegeEnabled};
				var tokenPrivileges = new TokenPrivileges {PrivilegeCount = 1, Privileges = new[] {privilege}};

				if (!NativeMethods.AdjustTokenPrivileges(hToken, false, ref tokenPrivileges, 0, IntPtr.Zero, IntPtr.Zero))
				{
					int hr = Marshal.GetHRForLastWin32Error();
					Marshal.ThrowExceptionForHR(hr);
				}
			}
			finally
			{
				if (!NativeMethods.CloseHandle(hToken))
				{
					int hr = Marshal.GetHRForLastWin32Error();
					Marshal.ThrowExceptionForHR(hr);
				}
			}
		}

		private static void RegistrySetOwner(string path, IdentityReference newOwner)
		{
			RegistryKey rk;
			using (
				rk =
					Registry.ClassesRoot.OpenSubKey(path, RegistryKeyPermissionCheck.ReadWriteSubTree,
						RegistryRights.TakeOwnership))
			{
				RegistrySecurity rs = rk.GetAccessControl();

				rs.SetOwner(newOwner);

				rk.SetAccessControl(rs);
			}

			using (
				rk =
					Registry.ClassesRoot.OpenSubKey(path, RegistryKeyPermissionCheck.ReadWriteSubTree,
						RegistryRights.ChangePermissions)
				)
			{
				RegistrySecurity rs = rk.GetAccessControl();

				rs.AddAccessRule(new RegistryAccessRule(newOwner, RegistryRights.FullControl,
					InheritanceFlags.ContainerInherit,
					PropagationFlags.None, AccessControlType.Allow));

				rk.SetAccessControl(rs);
			}
		}
	}
}