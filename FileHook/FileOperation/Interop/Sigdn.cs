namespace FileOperation.Interop
{
	public enum Sigdn
	{
		NormalDisplay = 0,
		ParentRelativeParsing = unchecked((int) 0x80018001),
		DesktopAbsoluteParsing = unchecked((int) 0x80028000),
		ParentRelativeEditing = unchecked((int) 0x80031001),
		DesktopAbsoluteEditing = unchecked((int) 0x8004c000),
		FilesysPath = unchecked((int) 0x80058000),
		Url = unchecked((int) 0x80068000),
		ParentRelativeForAddressBar = unchecked((int) 0x8007c001),
		ParentRelative = unchecked((int) 0x80080001),
		ParentRelativeForUi = unchecked((int) 0x80094001),
	}
}