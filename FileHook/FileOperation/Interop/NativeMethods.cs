﻿using System;
using System.Runtime.InteropServices;

namespace FileOperation.Interop
{
	internal static class NativeMethods
	{
		[DllImport("Ole32.dll", CharSet = CharSet.Unicode)]
		public static extern IntPtr CoLoadLibrary([MarshalAs(UnmanagedType.LPWStr)] string lpszLibName, bool bAutoFree);

		[DllImport("Kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr GetProcAddress(IntPtr hModule, [MarshalAs(UnmanagedType.LPStr)] string lpProcName);
	}
}