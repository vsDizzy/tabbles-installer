namespace FileOperation
{
	internal class OperationInternal
	{
		public string Destination;
		public int HResult;
		public bool IsDir;
		public string NewName;
		public string Path;
		public string Type;
	}
}