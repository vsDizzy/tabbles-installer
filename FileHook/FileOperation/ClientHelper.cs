using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Xml;
using System.Xml.Serialization;
using FileOperation.Interop;

namespace FileOperation
{
	internal static class ClientHelper
	{
		public static bool IsExplorerProcess()
		{
			using (Process p = Process.GetCurrentProcess())
			{
				string name = p.MainModule.FileName;
				string expected = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "Explorer.exe");

				return (name.Equals(expected, StringComparison.InvariantCultureIgnoreCase));
			}
		}

		public static bool IsSpecialItem(IShellItem item)
		{
			string path = item.GetDisplayName(Sigdn.FilesysPath);

			string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			if (path.StartsWith(appData, StringComparison.InvariantCultureIgnoreCase))
			{
				return true;
			}

			return false;
		}

		public static void Send(object obj)
		{
			const string pipeName = "TABBLES_PIPE_SERVER";
			try
			{
				Send(pipeName, obj);
			}
			catch
			{
				try
				{
					const string fallbackPipeName = "TABBLES_PIPE_SERVER_FALLBACK";
					Send(fallbackPipeName, obj);
				}
				catch
				{
				}
			}
		}

		private static void Send(string pipeName, object obj)
		{
			var xs = new XmlSerializer(obj.GetType());
			var ns = new XmlSerializerNamespaces(new[] {new XmlQualifiedName(string.Empty, "urn:default")});

			using (var pc = new NamedPipeClientStream(".", pipeName, PipeDirection.Out))
			{
				const int connectTimeout = 50;
				pc.Connect(connectTimeout);

				xs.Serialize(pc, obj, ns);
			}
		}
	}
}