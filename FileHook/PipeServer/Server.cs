﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Xml;
using System.Xml.Serialization;
using PipeServer.XmlEntities;

namespace PipeServer
{
	internal class Server
	{
		//private const string PipeName = @"TABBLES_PIPE_SERVER";
		private const string PipeName = @"TABBLES_PIPE_SERVER_FALLBACK";

		public string Read()
		{
			using (var ps = new NamedPipeServerStream(PipeName, PipeDirection.In))
			{
				Console.WriteLine("Waiting for connection...");
				ps.WaitForConnection();

				var xs = new XmlSerializer(typeof (Operation));
				object operation = xs.Deserialize(ps);
				return Serialize(operation);
			}
		}

		private string Serialize(object obj)
		{
			var xs = new XmlSerializer(obj.GetType());
			var ns = new XmlSerializerNamespaces(new[] {new XmlQualifiedName(string.Empty, "urn:default")});

			using (var sw = new StringWriter())
			{
				xs.Serialize(sw, obj, ns);
				return sw.ToString();
			}
		}
	}
}