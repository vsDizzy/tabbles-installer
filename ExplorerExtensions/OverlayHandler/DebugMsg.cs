﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace TabblesOverlayHandler
{
            
    public class DebugMsg
    {
        [XmlAttribute]
        public string msg { get; set; }
    }
}
