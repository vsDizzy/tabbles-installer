﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace TabblesOverlayHandler
{
    public class MessageCheckIfThisCachedValueWasCorrect
    {

        [XmlAttribute]
        public string path { get; set; }

        [XmlAttribute]
        public bool cachedValue { get; set; }
    }
}
