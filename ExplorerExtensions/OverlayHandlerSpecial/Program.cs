﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpShell.SharpIconOverlayHandler;
using OverlayHandlerCommon;


namespace OverlayHandlerSpecial
{
    public class TabblesOverlayHandlerSpecial : SharpIconOverlayHandler
    {

        private const int idHandler = 2;


        private bool initialized = false;
        private Dictionary<string, bool> cache = new Dictionary<string, bool>();


        protected override bool CanShowOverlay(string path, SharpShell.Interop.FILE_ATTRIBUTE attributes)
        {
            return Common.CanShowOverlay(path, ref initialized, cache, idHandler);
        }

        protected override System.Drawing.Icon GetOverlayIcon()
        {

            Common.log("getoverlayicon called", idHandler);
            return Resource1.locked;
        }

        protected override int GetPriority()
        {
            Common.log("getPriority called", idHandler);
            //  very low priority.
            return 90;
        }
    }
}
