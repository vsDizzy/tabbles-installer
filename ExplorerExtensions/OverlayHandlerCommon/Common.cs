﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Pipes;
using System.Xml.Linq;
using System.Runtime.InteropServices;

namespace OverlayHandlerCommon
{
    public static class Common
    {
        private static object gLock = new object();
        private static Random rand;

        private static string getLogFilePath(int idHandler)
        {
            var folderDocs = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            var tabblesFolder = System.IO.Path.Combine(folderDocs, "Tabbles");
            System.IO.Directory.CreateDirectory(tabblesFolder);
            return (System.IO.Path.Combine(tabblesFolder, "log_overlay_handler" + idHandler.ToString() + ".txt"));
        }

        public static void initIfNeeded(ref bool initialized, Dictionary<string, bool> cache, int idHandler)
        {

            bool mustBeInitialized;
            lock (gLock)
            {
                mustBeInitialized = !initialized;
                initialized = true;
            }


            if (mustBeInitialized)
            {
                Common.log("initializing.", idHandler);

                lock (gLock)
                {
                    //cache = new Dictionary<string, bool>();
                    rand = new Random();
                }

                // thread which deletes the log when it is too big
                ThreadUtils.execInThreadForceNewThread(idHandler, () =>
                {
                    
                    var logFilePath = getLogFilePath(idHandler);

                    while (true)
                    {

                        try
                        {

                            var fi = new System.IO.FileInfo(logFilePath);
                            if (fi.Length > 1000000)
                            {
                                System.IO.File.Delete(logFilePath);
                            }

                            // 10 minutes
                            System.Threading.Thread.Sleep(1000 * 60 * 10);


                        }
                        catch
                        {
                            // probably an access problem (some other thread might be writing to the log).
                            System.Threading.Thread.Sleep(2000); // retry in 2 seconds

                        }
                    }
                });
                ThreadUtils.execInThreadForceNewThread(idHandler, () =>
                {
                    //SendToPipe(new DebugMsg { msg = "overlay handler : In thread. 1." });
                    while (true)
                    {
                        try
                        {
                            using (var pipeServer = new System.IO.Pipes.NamedPipeServerStream(@"TABBLES_OVERLAY_HANDLER_PIPE_SERVER" + idHandler.ToString(),
                                                                        System.IO.Pipes.PipeDirection.In,
                                                                        -1,
                                                                        PipeTransmissionMode.Message,   // Message-based communication
                                                                        PipeOptions.None))
                            {

                                pipeServer.WaitForConnection(); // blocks this thread.

                                var xd = XDocument.Load(pipeServer);

                                ThreadUtils.execInThreadForceNewThread(idHandler, () =>
                                {
                                    Common.log("<-  got message from tabbles: " + xd.ToString() + " <-", idHandler);
                                    var root = xd.Root;
                                    if (root.Name == "update_your_cache")
                                    {
                                        foreach (var xelFile in root.Elements("file"))
                                        {
                                            var path = xelFile.Attribute("path").Value;
                                            var correctValue = Boolean.Parse(xelFile.Attribute("correct_value").Value);

                                            lock (gLock)
                                            {
                                                cache[path] = correctValue;
                                            }
                                            //log("cache updated: " + path + ", correct value = " + correctValue.ToString());

                                            IntPtr ptr = Marshal.StringToHGlobalUni(path);
                                            Shell.SHChangeNotify(Shell.HChangeNotifyEventID.SHCNE_UPDATEITEM, Shell.HChangeNotifyFlags.SHCNF_PATHW | Shell.HChangeNotifyFlags.SHCNF_FLUSH, ptr, IntPtr.Zero);
                                            Marshal.FreeHGlobal(ptr);

                                        }

                                    }
                                    else
                                    {
                                        Common.log(">>>>message from tabbles unrecognized: " + xd.ToString(), idHandler);

                                    }

                                });

                            }
                        }
                        catch (Exception e)
                        {
                            Common.log(">>>  exception:" + e.GetType().ToString() + ", " + e.Message, idHandler);

                        }

                    }
                });





            }

        }



        public static void log(string txt, int idHandler)
        {
            try
            {
                
                var logFilePath = getLogFilePath(idHandler);

                using (var sw = System.IO.File.AppendText(logFilePath))
                {
                    sw.WriteLine(txt + System.Environment.NewLine);
                }
            }
            catch { }
        }


        public static void retryOnPipeError(Action a, Random rand, int idHandler)
        {
            var count = 0;
            var canExit = false;
            while (!canExit)
            {
                try
                {
                    a.Invoke();
                    canExit = true;
                }
                catch (Exception e)
                {
                    if (e is TimeoutException || e is System.IO.IOException)
                    {

                        System.Threading.Thread.Sleep(200 - (rand.Next() % 50));


                        count++;
                        if (count >= 3)
                        {
                            log(">> stopped retrying! ", idHandler);
                            canExit = true;
                        }
                        else
                        {
                            log("retrying " + count.ToString(), idHandler);
                        }
                    }
                    else
                    {
                        throw;
                    }

                }
            }


        }



        public static bool CanShowOverlay(string path, ref bool initialized, Dictionary<string, bool> cache, int idHandler)
        {
            

            try
            {
                Common.initIfNeeded(ref initialized, cache, idHandler);



                bool cachedValue;
                lock (gLock)
                {
                    if (cache.ContainsKey(path))
                    {
                        cachedValue = cache[path];
                    }
                    else
                    {
                        cachedValue = false;
                    }
                }



                Common.log("canShowOverlay called for: " + path + ". responding with cached value = " + cachedValue.ToString(), idHandler);

                // since the cache could be absent or outdated, here we must 
                // ask Tabbles to update the cache with the correct value, and then redraw the file.

                ThreadUtils.execInThreadForceNewThread(idHandler, () =>
                {


                    var xatPath = new XAttribute("path", path);
                    var xatCachedValue = new XAttribute("cached_value", cachedValue);
                    var attrs = new object[] { xatPath, xatCachedValue };
                    var xel = new XElement("tell_me_if_cache_is_correct", attrs);
                    var xd = new XDocument(xel);



                    try
                    {
                        Common.retryOnPipeError(() =>
                        {
                            lock (gLock) // only one thread at a time must attempt this. Otherwise I get many retries in retryOnPipeError!!
                            {
                                string pipeName = @"TABBLES_PIPE_SERVER_OVERLAY" + idHandler.ToString();
                                using (var pipeClient = new NamedPipeClientStream(".", pipeName, PipeDirection.Out))
                                {
                                    pipeClient.Connect(50);
                                    xd.Save(pipeClient);
                                }
                            }
                        }, rand, idHandler);
                        Common.log("-> sent message to tabbles: tell me if this cached value is correct: " + path + " , cached value = " + cachedValue.ToString() + " ->", idHandler);
                    }
                    catch (Exception e)
                    {
                        Common.log(">>> exception sending to tabbles: " + e.GetType().ToString() + " --- " + e.Message, idHandler);
                    }




                });




                // quicky return the value from the cache. This value could be outdated, but we'll deal with this later.
                return cachedValue;

            }
            catch (Exception e)
            {
                Common.log(">>> exception in CanShowOverlay: " + e.GetType().ToString() + " --- " + e.Message, idHandler);
                return false;
            }
        }

    }
}